#include "color_button.h"
#include <QColorDialog>

ColorButton::ColorButton(QWidget * parent) : QPushButton(parent)
{
    connect(this, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    setColor(QColor(255, 0, 0));
}

void ColorButton::setColor(const QColor& c)
{
    m_color = c;
    QPixmap pixmap(12,12);
    pixmap.fill(m_color);
    setIcon(pixmap);
}

QColor ColorButton::color()
{
    return m_color;
}

void ColorButton::buttonClicked() 
{
    QColor color = QColorDialog::getColor(m_color, this);
    if ( color.isValid() )
    {
        setColor(color);
        emit colorChanged(color);
    }
}

