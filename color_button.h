#ifndef COLOR_BUTTON_H
#define COLOR_BUTTON_H

#include <QColor>
#include <QPushButton>

class ColorButton : public QPushButton
{
    Q_OBJECT

public:
    ColorButton(QWidget * parent = 0);
    void setColor(const QColor& c);
    QColor color();


private slots:
    void buttonClicked();
signals:
    void colorChanged(QColor);
private: 
    QColor m_color;

};


#endif //COLOR_BUTTON_H
