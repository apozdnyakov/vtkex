#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "QVTKWidget.h"
#include "vtkFileOutputWindow.h"
#include <vtkPolyDataMapper.h>
#include <vtkGeometryFilter.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTransformFilter.h>
#include <vtkTransform.h>
#include <vtkProperty.h>
#include <vtkCursor3D.h>
#include "vtkCylinderSource.h"
#include <vtkPointData.h>
#include <vtkQuad.h>
#include <vtkTriangle.h>

#include <VtkInit.h>

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_renderer(0),
    m_axesActor(0),
    m_markerWidget(0),
    m_cylinder(0),
    m_cylinderActor(0),
    m_polydata(0)
{
    ui->setupUi(this);
    ui->colorButton->setColor(Qt::red);
    ui->backColorButton->setColor(Qt::white);

    m_vtkWidget = ui->qvtkWidget;
    vtkSmartPointer<vtkFileOutputWindow> w = vtkSmartPointer<vtkFileOutputWindow>::New();
    w->SetFileName("vtk_errors.txt");
    w->SetInstance(w);

    m_renderer = vtkRenderer::New();
    m_renderer->SetBackground(1, 1, 1);
    QColor bcol = ui->backColorButton->color();
    m_renderer->SetBackground(bcol.redF(), bcol.greenF(), bcol.blueF());

    m_vtkWidget->GetRenderWindow()->AddRenderer(m_renderer);

    m_axesActor = vtkAxesActor::New();

    m_markerWidget = vtkOrientationMarkerWidget::New();
    m_markerWidget->SetOutlineColor( 0.9300, 0.5700, 0.1300 );
    m_markerWidget->SetInteractor( m_vtkWidget->GetRenderWindow()->GetInteractor() );
    m_markerWidget->SetOrientationMarker( m_axesActor );
    m_markerWidget->SetViewport( 0.0, 0.0, 0.2, 0.2 );
    m_markerWidget->SetEnabled( 1 );
    m_markerWidget->InteractiveOn();

    m_polydata = vtkPolyData::New();

    createCylinder();
//    m_cylinder = vtkCylinderSource::New();
//    m_cylinder->SetResolution(ui->resolutionBox->value());

    vtkPolyDataMapper *cylinderMapper = vtkPolyDataMapper::New();
    cylinderMapper->SetInputData(m_polydata);
    m_cylinderActor = vtkActor::New();
    m_cylinderActor->SetMapper(cylinderMapper);
//    QColor col = ui->colorButton->color();
//    m_cylinderActor->GetProperty()->SetColor(col.redF(), col.greenF(), col.blueF());
//    m_cylinderActor->RotateX(30.0);
//    m_cylinderActor->RotateY(-45.0);
    m_renderer->AddActor(m_cylinderActor);

    m_renderer->ResetCamera();

    m_vtkWidget->GetRenderWindow()->Render();

    connect(ui->resolutionBox, SIGNAL(valueChanged(int)), this, SLOT(resolutionChanged()));
    connect(ui->colorButton, SIGNAL(colorChanged(QColor)), this, SLOT(colorChanged()));
    connect(ui->backColorButton, SIGNAL(colorChanged(QColor)), this, SLOT(backgroundChanged()));
    connect(ui->xBox, SIGNAL(valueChanged(double)), this, SLOT(coordChanged()));
    connect(ui->yBox, SIGNAL(valueChanged(double)), this, SLOT(coordChanged()));
    connect(ui->zBox, SIGNAL(valueChanged(double)), this, SLOT(coordChanged()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resolutionChanged()
{
    createCylinder();
    m_vtkWidget->GetRenderWindow()->Render();
}

void MainWindow::colorChanged()
{
    createCylinder();
    m_vtkWidget->GetRenderWindow()->Render();
}

void MainWindow::backgroundChanged()
{
    QColor bcol = ui->backColorButton->color();
    m_renderer->SetBackground(bcol.redF(), bcol.greenF(), bcol.blueF());
    m_vtkWidget->GetRenderWindow()->Render();
}

void MainWindow::coordChanged()
{
    createCylinder();
    m_vtkWidget->GetRenderWindow()->Render();
}

void MainWindow::createCylinder()
{
    int num = ui->resolutionBox->value();
    QColor col = ui->colorButton->color();

    double pX = ui->xBox->value();
    double pY = ui->yBox->value();
    double pZ = ui->zBox->value();

    double height = 1.0;
    double radius = 1.0*0.5;

    VTK_CREATE(vtkPoints, points);

    VTK_CREATE(vtkUnsignedCharArray, colors);
    colors->SetNumberOfComponents(4);
    colors->SetName("Colors");

    VTK_CREATE(vtkCellArray, cells);

    double deltaAngle = M_PI/180.0*360.0/num;
    double deltaH = height/(num-1);

    double minDist = FLT_MAX;
    double maxDist = -FLT_MAX;
    QVector<double> dists;

    for(int h = 0; h < num; ++h)
    {
        for(int a = 0; a < num; ++a)
        {
            double angle = deltaAngle*a;
            double x = radius*cos(angle);
            double y = radius*sin(angle);
            double z = -0.5*height+deltaH*h;
            points->InsertNextPoint(x, y, z);
            double dx = x-pX;
            double dy = y-pY;
            double dz = z-pZ;
            double dist = sqrt(dx*dx+dy*dy+dz*dz);
            minDist = qMin(minDist, dist);
            maxDist = qMax(maxDist, dist);
            dists.push_back(dist);
        }
    }

    {
        double x = 0;
        double y = 0;
        double z = -height*0.5;
        points->InsertNextPoint(x, y, z);
        double dx = x-pX;
        double dy = y-pY;
        double dz = z-pZ;
        double dist = sqrt(dx*dx+dy*dy+dz*dz);
        minDist = qMin(minDist, dist);
        maxDist = qMax(maxDist, dist);
        dists.push_back(dist);
    }
    {
        double x = 0;
        double y = 0;
        double z = height*0.5;
        points->InsertNextPoint(x, y, z);
        double dx = x-pX;
        double dy = y-pY;
        double dz = z-pZ;
        double dist = sqrt(dx*dx+dy*dy+dz*dz);
        minDist = qMin(minDist, dist);
        maxDist = qMax(maxDist, dist);
        dists.push_back(dist);
    }

    foreach(const double & dist, dists)
    {
        //unsigned char c[4] = {col.red(), col.green(), col.blue(), 255*((dist-minDist)/(maxDist-minDist))};
        colors->InsertNextTuple4( col.red(), col.green(), col.blue(), 255*((dist-minDist)/(maxDist-minDist)) );
    }

    for(int h = 0; h < num-1; ++h)
    {
        for(int a = 0; a < num; ++a)
        {
            int b = a==num-1 ? 0 : (a+1);
            VTK_CREATE(vtkQuad, cell);
            cell->GetPointIds()->SetId(0, h*num+a);
            cell->GetPointIds()->SetId(1, h*num+b);
            cell->GetPointIds()->SetId(2, h*num+b+num);
            cell->GetPointIds()->SetId(3, h*num+a+num);
            cells->InsertNextCell(cell);
        }
    }

    for(int a = 0; a < num; ++a)
    {
        int b = a==num-1 ? 0 : (a+1);
        VTK_CREATE(vtkTriangle, cell);
        cell->GetPointIds()->SetId(0, num*num);
        cell->GetPointIds()->SetId(1, b);
        cell->GetPointIds()->SetId(2, a);
        cells->InsertNextCell(cell);
    }
    for(int a = 0; a < num; ++a)
    {
        int b = a==num-1 ? 0 : (a+1);
        VTK_CREATE(vtkTriangle, cell);
        cell->GetPointIds()->SetId(0, num*num+1);
        cell->GetPointIds()->SetId(1, num*(num-1)+a);
        cell->GetPointIds()->SetId(2, num*(num-1)+b);
        cells->InsertNextCell(cell);
    }

    m_polydata->SetPoints(points);
    m_polydata->SetPolys(cells);
    m_polydata->GetPointData()->SetScalars(colors);
}
