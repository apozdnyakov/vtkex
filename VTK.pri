######################################################################
# VTK Libraries and Headers
######################################################################

windows* {
INCLUDEPATH       += ../vtk8-release/include/vtk-8.1

CONFIG(release, release|debug){
    VTK_LIB_DIR      = ../vtk8-release/lib
}else{
    VTK_LIB_DIR      = ../vtk8-debug/lib
}

#LIBS += -ladvapi32
}

windows* {
LIBS              += -L$$VTK_LIB_DIR -lvtkCommonCore-8.1 -lvtkCommonSystem-8.1 -lvtkCommonMath-8.1 -lvtkImagingMath-8.1 -lvtkImagingFourier-8.1 -lvtkalglib-8.1 -lvtkFiltersHybrid-8.1 -lvtkCommonColor-8.1 -lvtkCommonComputationalGeometry-8.1 -lvtkCommonDataModel-8.1 -lvtkCommonExecutionModel-8.1 -lvtkCommonMisc-8.1 -lvtkCommonTransforms-8.1 -lvtkInfovisCore-8.1\
                     -lvtkFiltersCore-8.1 -lvtkFiltersGeneral-8.1 -lvtkFiltersGeneric-8.1 -lvtkFiltersImaging-8.1 -lvtkFiltersModeling-8.1 -lvtkFiltersStatistics-8.1 -lvtkFiltersSources-8.1 -lvtkFiltersGeometry-8.1 -lvtkFiltersExtraction-8.1\
                     -lvtkChartsCore-8.1  -lvtkRenderingLOD-8.1\
#                    -lvtkglew-8.1 \
                     -lvtkImagingCore-8.1 -lvtkImagingSources-8.1 -lvtkImagingGeneral-8.1 \
                     -lvtkIOCore-8.1 -lvtkIOImport-8.1 -lvtkIOImage-8.1  -lvtkIOXML-8.1 \
                     -lvtkRenderingCore-8.1 -lvtkRenderingVolume-8.1 -lvtkRenderingImage-8.1 -lvtkRenderingContext2D-8.1 -lvtkRenderingQt-8.1 -lvtkRenderingOpenGL2-8.1 -lvtkRenderingVolumeOpenGL2-8.1 -lvtkRenderingFreeType-8.1 -lvtkRenderingAnnotation-8.1 \
                     -lvtkInteractionImage-8.1 -lvtkInteractionWidgets-8.1 -lvtkInteractionStyle-8.1 -lvtkGUISupportQt-8.1 \
                     -lvtkViewsCore-8.1 -lvtkViewsContext2D-8.1 -lvtkViewsQt-8.1 \
                     -lvtkexpat-8.1 -lvtkpng-8.1 -lvtktiff-8.1 -lvtkjpeg-8.1 -lvtkzlib-8.1 -lvtksys-8.1 -lvtkfreetype-8.1
}

linux* {

INCLUDEPATH       += ../vtk812-install/include/vtk-8.1/

LIBS              +=  -L../vtk812-install/lib -lvtkCommonCore-8.1 -lvtkCommonSystem-8.1 -lvtkCommonMath-8.1 \
                     -lvtkImagingMath-8.1 -lvtkImagingFourier-8.1 -lvtkalglib-8.1 -lvtkFiltersHybrid-8.1 -lvtkCommonColor-8.1 \
                     -lvtkglew-8.1 -lvtkCommonComputationalGeometry-8.1 -lvtkCommonDataModel-8.1 -lvtkCommonExecutionModel-8.1 \
                     -lvtkCommonMisc-8.1 -lvtkCommonTransforms-8.1 -lvtkInfovisCore-8.1\
                     -lvtkFiltersCore-8.1 -lvtkFiltersGeneral-8.1 -lvtkFiltersGeneric-8.1 -lvtkFiltersImaging-8.1 \
                     -lvtkFiltersModeling-8.1 -lvtkFiltersStatistics-8.1 -lvtkFiltersSources-8.1 -lvtkFiltersGeometry-8.1 \
                     -lvtkFiltersExtraction-8.1\
                     -lvtkChartsCore-8.1 -lvtkRenderingLOD-8.1\
                     -lvtkImagingCore-8.1 -lvtkImagingSources-8.1 -lvtkImagingGeneral-8.1 \
                     -lvtkIOCore-8.1 -lvtkIOImport-8.1 -lvtkIOImage-8.1  -lvtkIOXML-8.1 \
                     -lvtkRenderingCore-8.1 -lvtkRenderingVolume-8.1 -lvtkRenderingImage-8.1 -lvtkRenderingContext2D-8.1 \
                     -lvtkRenderingQt-8.1 -lvtkRenderingOpenGL2-8.1 -lvtkRenderingVolumeOpenGL2-8.1 -lvtkRenderingFreeType-8.1 \
                     -lvtkRenderingAnnotation-8.1 \
                     -lvtkInteractionImage-8.1 -lvtkInteractionWidgets-8.1 -lvtkInteractionStyle-8.1 -lvtkGUISupportQt-8.1 \
                     -lvtkViewsCore-8.1 -lvtkViewsContext2D-8.1 -lvtkViewsQt-8.1 \
                     -lvtkexpat-8.1 -lvtkpng-8.1 -lvtktiff-8.1 -lvtkjpeg-8.1 -lvtkzlib-8.1 -lvtksys-8.1 -lvtkfreetype-8.1

}

QT                += opengl

HEADERS += VtkInit.h
message(VTK.pri included)
