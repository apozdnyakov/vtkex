#ifndef VTKINIT_H
#define VTKINIT_H

#include <vtkAutoInit.h>
//#ifdef Q_OS_WIN
//VTK_MODULE_INIT(vtkRenderingOpenGL);
//VTK_MODULE_INIT(vtkRenderingVolumeOpenGL);
//#else
VTK_MODULE_INIT(vtkRenderingOpenGL2);
VTK_MODULE_INIT(vtkRenderingVolumeOpenGL2);
//#endif
VTK_MODULE_INIT(vtkRenderingFreeType);
//VTK_MODULE_INIT(vtkRenderingFreeTypeOpenGL);
VTK_MODULE_INIT(vtkInteractionStyle);
//VTK_MODULE_INIT(vtkOrientationMarkerWidget);

#endif // VTKINIT_H
