#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "vtkSmartPointer.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class vtkActor;
class vtkAxesActor;
class vtkOrientationMarkerWidget;
class QVTKWidget;
class vtkRenderer;
class vtkCylinderSource;
class vtkPolyData;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void resolutionChanged();
    void colorChanged();
    void backgroundChanged();
    void coordChanged();
private:
    void createCylinder();
private:
    Ui::MainWindow *ui;
    QVTKWidget * m_vtkWidget;
    vtkRenderer * m_renderer;
    vtkSmartPointer<vtkAxesActor> m_axesActor;
    vtkOrientationMarkerWidget * m_markerWidget;
    vtkSmartPointer<vtkCylinderSource> m_cylinder;
    vtkSmartPointer<vtkActor> m_cylinderActor;
    vtkSmartPointer<vtkPolyData> m_polydata;
};

#endif // MAINWINDOW_H
